package at.danidipp.aau;

import at.omi.smarthome.interfaces.Actor;
import at.omi.smarthome.interfaces.Device;
import at.omi.smarthome.simulation2.SimulatedLight2;

public class SL2Adapter extends SimulatedLight2 implements Device, Actor {
    private String id;
    private String name;

    public SL2Adapter(String id, String name) {
        super();
        this.id = id;
        this.name = name;
    }


    @Override
    public void switchOn() {
        set(1);
    }

    @Override
    public void switchOff() {
        set(0);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
