package at.danidipp.aau;

import at.omi.smarthome.interfaces.Actor;
import at.omi.smarthome.interfaces.Device;
import at.omi.smarthome.simulation1.SimulatedLight1;

public class SL1Adapter extends SimulatedLight1 implements Device, Actor {
    private String id;
    private String name;

    public SL1Adapter(String id, String name) {
        super();
        this.id = id;
        this.name = name;
    }


    @Override
    public void switchOn() {
        sendCommand("CONTROL;STATUS;ON");
    }

    @Override
    public void switchOff() {
        sendCommand("CONTROL;STATUS;OFF");
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
