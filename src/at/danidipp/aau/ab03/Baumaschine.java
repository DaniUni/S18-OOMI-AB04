package at.danidipp.aau.ab03;

public interface Baumaschine {
    public String getName();
    public void setName(String name);
    public int getLeistung();
    public void setLeistung(int leistung);
    public double getGeschwindigkeit();
    public void setGeschwindigkeit(double geschwindigkeit);
    public double getGewicht();
    public void setGewicht(double gewicht);
    public default void druckeBeschreibung(){
        System.out.println("Name: "+getName());
        System.out.println("Leistung: "+getLeistung());
        System.out.println("Geschwindigkeit: "+getGeschwindigkeit());
        System.out.println("Gewicht: "+getGewicht());
    }
}
