package at.danidipp.aau.ab03;

public abstract class BaumaschineImpl implements Baumaschine {

    private String name;
    private int leistung;
    private double geschwindigkeit;
    private double gewicht;

    public BaumaschineImpl(String name, int leistung, double geschwindigkeit, double gewicht) {
        this.name = name;
        this.leistung = leistung;
        this.geschwindigkeit = geschwindigkeit;
        this.gewicht = gewicht;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getLeistung() {
        return leistung;
    }

    @Override
    public void setLeistung(int leistung) {
        this.leistung = leistung;
    }

    @Override
    public double getGeschwindigkeit() {
        return geschwindigkeit;
    }

    @Override
    public void setGeschwindigkeit(double geschwindigkeit) {
        this.geschwindigkeit = geschwindigkeit;
    }

    @Override
    public double getGewicht() {
        return gewicht;
    }

    @Override
    public void setGewicht(double gewicht) {
        this.gewicht = gewicht;
    }

}
