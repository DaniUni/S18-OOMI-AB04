package at.danidipp.aau.ab03;

public class BaggerLader2 extends BaggerImpl implements Bagger, Lader{
    //Baumaschine
//    private String name;
//    private int leistung;
//    private double geschwindigkeit;
//    private double gewicht;

    //Bagger
//    private double grabtiefe;
//    private double reichweite;

    //Lader
    private double schaufelvolumen;
    private double kipphoehe;

    public BaggerLader2(String name, int leistung, double geschwindigkeit, double gewicht, double grabtiefe, double reichweite, double schaufelvolumen, double kipphoehe) {
        //Baumaschine
//        this.name = name;
//        this.leistung = leistung;
//        this.geschwindigkeit = geschwindigkeit;
//        this.gewicht = gewicht;

        //Bagger
//        this.grabtiefe = grabtiefe;
//        this.reichweite = reichweite;
        super(name, leistung, geschwindigkeit, gewicht, grabtiefe, reichweite);

        //Lader
        this.schaufelvolumen = schaufelvolumen;
        this.kipphoehe = kipphoehe;
    }

    //Baumaschine
    /**
    @Override
    public String getName() {
        return name;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public int getLeistung() {
        return leistung;
    }
    @Override
    public void setLeistung(int leistung) {
        this.leistung = leistung;
    }
    @Override
    public double getGeschwindigkeit() {
        return geschwindigkeit;
    }
    @Override
    public void setGeschwindigkeit(double geschwindigkeit) {
        this.geschwindigkeit = geschwindigkeit;
    }
    @Override
    public double getGewicht() {
        return gewicht;
    }
    @Override
    public void setGewicht(double gewicht) {
        this.gewicht = gewicht;
    }
    **/

    //Bagger
    /**
    @Override
    public double getGrabtiefe() {
        return grabtiefe;
    }
    @Override
    public void setGrabtiefe(double grabtiefe) {
        this.grabtiefe = grabtiefe;
    }
    @Override
    public double getReichweite() {
        return reichweite;
    }
    @Override
    public void setReichweite(double reichweite) {
        this.reichweite = reichweite;
    }
    **/

    //Lader
    @Override
    public double getSchaufelvolumen() {
        return schaufelvolumen;
    }
    @Override
    public void setSchaufelvolumen(double schaufelvolumen) {
        this.schaufelvolumen = schaufelvolumen;
    }
    @Override
    public double getKipphoehe() {
        return kipphoehe;
    }
    @Override
    public void setKipphoehe(double kipphoehe) {
        this.kipphoehe = kipphoehe;
    }


    @Override
    public void druckeBeschreibung() {
        //Baumaschine.super.druckeBeschreibung();
//        System.out.println("Name: "+getName());
//        System.out.println("Leistung: "+getLeistung());
//        System.out.println("Geschwindigkeit: "+getGeschwindigkeit());
//        System.out.println("Gewicht: "+getGewicht());

//        Bagger.super.druckeBeschreibung();
        super.druckeBeschreibung();
        Lader.super.druckeBeschreibung();
    }
}
