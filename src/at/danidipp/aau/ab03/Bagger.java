package at.danidipp.aau.ab03;

public interface Bagger extends Baumaschine {
    public final double MAX_GRABTIEFE = 15;
    public final double MAX_REICHWEITE = 18;

    public double getGrabtiefe();
    public void setGrabtiefe(double grabtiefe);
    public double getReichweite();
    public void setReichweite(double reichweite);
    public default void druckeBeschreibung(){
        System.out.println("Grabtiefe: "+getGrabtiefe());
        System.out.println("Reichweite: "+getReichweite());
    }
}
