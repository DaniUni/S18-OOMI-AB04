package at.danidipp.aau;

import at.danidipp.aau.ab02.Ab02;
import at.omi.smarthome.gui.SmarthomeGUI;
import at.omi.smarthome.interfaces.Actor;
import at.omi.smarthome.interfaces.Device;
import at.omi.smarthome.simulation1.SimulatedLight1;
import at.omi.smarthome.simulation2.SimulatedLight2;

import java.awt.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {

                    //SimulatedLight1 sl1 = new SimulatedLight1();
                    //SimulatedLight2 sl2 = new SimulatedLight2();

                    ArrayList<Device> devices = new ArrayList<Device>();

                    Actor a1 = new SL1Adapter("532521", "Lampe Wohnzimmer");
                    Actor a2 = new SL2Adapter("552521", "Licht Bad");

                    devices.add(a1);
                    devices.add(a2);

                    SmarthomeGUI smarthome = new SmarthomeGUI(devices);
                    smarthome.setVisible(true);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
