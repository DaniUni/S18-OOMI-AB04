package at.danidipp.aau.ab02;

import java.util.ArrayList;

public class Car extends Vehicle implements CombustionEngine{
    private double fuel;
    private double maxFuel;
    private double fuelConsumption;

    public Car(String name, Brand brand, ArrayList<Workshop> workshops, int weight, int maxPermissableWeight, double maxSpeed, double maxFuel, double fuelConsumption){
        super(name, brand, workshops,weight, maxPermissableWeight, maxSpeed);
        this.fuel = 0;
        this.maxFuel = maxFuel;
        this.fuelConsumption = fuelConsumption;
    }

    @Override
    public void printInfo(){
        super.printInfo();
        CombustionEngine.super.printInfo();
    }

    public double getFuel() {
        return fuel;
    }
    public void setFuel(double fuel) {
        this.fuel = fuel;
    }
    public double getMaxFuel() {
        return maxFuel;
    }
    public void setMaxFuel(double maxFuel) {
        this.maxFuel = maxFuel;
    }
    public double getFuelConsumption() {
        return fuelConsumption;
    }
    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }
}
