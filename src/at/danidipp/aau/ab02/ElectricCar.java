package at.danidipp.aau.ab02;

import java.util.ArrayList;

public class ElectricCar extends Vehicle implements ElectricEngine{
    private double batteryLevel;
    private double maxBatteryCapacity;
    private double powerConsumption;

    public ElectricCar(String name, Brand brand, ArrayList<Workshop> workshops, int weight, int maxPermissableWeight, double maxSpeed, double maxBatteryCapacity, double powerConsumption) {
        super(name, brand, workshops, weight, maxPermissableWeight, maxSpeed);
        this.batteryLevel = 0;
        this.maxBatteryCapacity = maxBatteryCapacity;
        this.powerConsumption = powerConsumption;
    }

    @Override
    public void printInfo(){
        super.printInfo();
        ElectricEngine.super.printInfo();
    }

    public double getBatteryLevel() {
        return batteryLevel;
    }
    public void setBatteryLevel(double batteryLevel) {
        this.batteryLevel = batteryLevel;
    }
    public double getMaxBatteryCapacity() {
        return maxBatteryCapacity;
    }
    public void setMaxBatteryCapacity(double maxBatteryCapacity) {
        this.maxBatteryCapacity = maxBatteryCapacity;
    }
    public double getPowerConsumption() {
        return powerConsumption;
    }
    public void setPowerConsumption(double powerConsumption) {
        this.powerConsumption = powerConsumption;
    }
}
