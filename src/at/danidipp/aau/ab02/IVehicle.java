package at.danidipp.aau.ab02;

import java.util.ArrayList;

public interface IVehicle {

    public double accelerate();
    public double brake();

    public default void drive(int kilometers) {
        for (int i = 0; i < kilometers; i++) {
            accelerate();
            accelerate();
            accelerate();
            brake();
            brake();
            brake();
            keepDriving(i);
        }
    }
    public boolean keepDriving(int distanceDriven);

    public default void printInfo(){
        System.out.println("Vehicle #"+getId()+": \""+getName()+"\" by "+getBrand().getName()+".");
        System.out.println("Available workshops:");
        for(Workshop w : getWorkshops()) System.out.println(w.getName());
        System.out.println("Current weight: "+getWeight()+", max weight: "+ getMaxPermissableWeight());
        System.out.println("Current speed: "+getSpeed()+", max speed: "+getMaxSpeed());
    }

    //getters and setters
    public long getId();
    public void setId(long id);
    public String getName();
    public void setName(String name);
    public Brand getBrand();
    public void setBrand(Brand brand);
    public ArrayList<Workshop> getWorkshops();
    public void setWorkshops(ArrayList<Workshop> workshops);
    public int getWeight();
    public void setWeight(int weight);
    public int getMaxPermissableWeight();
    public void setMaxPermissableWeight(int maxPermissableWeight);
    public double getSpeed();
    public void setSpeed(double speed);
    public double getMaxSpeed();
    public void setMaxSpeed(double maxSpeed);
}
