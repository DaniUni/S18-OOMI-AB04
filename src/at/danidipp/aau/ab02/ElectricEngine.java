package at.danidipp.aau.ab02;

public interface ElectricEngine extends IVehicle{

    public double getBatteryLevel();
    public void setBatteryLevel(double batteryLevel);
    public double getMaxBatteryCapacity();
    public void setMaxBatteryCapacity(double maxBatteryCapacity);
    public double getPowerConsumption();
    public void setPowerConsumption(double powerConsumption);

    public default void charge(double power, double hours){
        setBatteryLevel(power*hours);
        if(getBatteryLevel() > getMaxBatteryCapacity()) setBatteryLevel(getMaxBatteryCapacity());
    }

    @Override
    public default double brake(){
        if(getSpeed()>0) setBatteryLevel(getBatteryLevel()+0.001);
        double newSpeed = Math.max(getSpeed()-10, 0);
        setSpeed(newSpeed);
        return newSpeed;
    }

    @Override
    public default boolean keepDriving(int distanceDriven){
        if (getBatteryLevel() >= getPowerConsumption()/100) setBatteryLevel(getBatteryLevel()-getPowerConsumption()/100);
        else {
            System.out.println("Out of power! Drove " + distanceDriven + " kilometers.");
            return false;
        }
        return true;
    }

    public default void printInfo(){
        System.out.println("Current charge: "+getBatteryLevel()+", max charge: "+getMaxBatteryCapacity()+", power consumption: "+getPowerConsumption());
    }
}
