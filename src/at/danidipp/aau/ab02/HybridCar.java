package at.danidipp.aau.ab02;

import java.util.ArrayList;

public class HybridCar extends Vehicle implements CombustionEngine, ElectricEngine {
    //Car
    private double fuel;
    private double maxFuel;
    private double fuelConsumption;

    //ElectricCar
    private double batteryLevel;
    private double maxBatteryCapacity;
    private double powerConsumption;

    public HybridCar(String name, Brand brand, ArrayList<Workshop> workshops, int weight, int maxPermissableWeight, double maxSpeed, double fuel, double maxFuel, double fuelConsumption, double batteryLevel, double maxBatteryCapacity, double powerConsumption) {
        super(name, brand, workshops, weight, maxPermissableWeight, maxSpeed);

        this.fuel = fuel;
        this.maxFuel = maxFuel;
        this.fuelConsumption = fuelConsumption;

        this.batteryLevel = batteryLevel;
        this.maxBatteryCapacity = maxBatteryCapacity;
        this.powerConsumption = powerConsumption;
    }

    @Override
    public boolean keepDriving(int distanceDriven) {
        return CombustionEngine.super.keepDriving(distanceDriven) && ElectricEngine.super.keepDriving(distanceDriven);
    }

    public double getFuel() {
        return fuel;
    }
    public void setFuel(double fuel) {
        this.fuel = fuel;
    }
    public double getMaxFuel() {
        return maxFuel;
    }
    public void setMaxFuel(double maxFuel) {
        this.maxFuel = maxFuel;
    }
    public double getFuelConsumption() {
        return fuelConsumption;
    }
    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public double getBatteryLevel() {
        return batteryLevel;
    }
    public void setBatteryLevel(double batteryLevel) {
        this.batteryLevel = batteryLevel;
    }
    public double getMaxBatteryCapacity() {
        return maxBatteryCapacity;
    }
    public void setMaxBatteryCapacity(double maxBatteryCapacity) {
        this.maxBatteryCapacity = maxBatteryCapacity;
    }
    public double getPowerConsumption() {
        return powerConsumption;
    }
    public void setPowerConsumption(double powerConsumption) {
        this.powerConsumption = powerConsumption;
    }
}
