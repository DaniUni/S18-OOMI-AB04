package at.danidipp.aau.ab02;

import java.util.ArrayList;

public abstract class Vehicle implements IVehicle{
    private long id;
    private String name;
    private Brand brand;
    private ArrayList<Workshop> workshops;
    private int weight;
    private int maxPermissableWeight;
    private double speed;
    private double maxSpeed;

    private static long nextId = 0;

    public Vehicle(String name, Brand brand, ArrayList<Workshop> workshops, int weight, int maxPermissableWeight, double maxSpeed) {
        this.id = getNextId();
        this.name = name;
        this.brand = brand;
        this.workshops = workshops;
        this.weight = weight;
        this.maxPermissableWeight = maxPermissableWeight;
        this.speed = 0;
        this.maxSpeed = maxSpeed;
    }

    public double accelerate(){
        return (speed+10) > maxSpeed ? speed : speed+10;
    }

    public double brake(){
        return speed = Math.max(speed-10, 0);
    }

    /**Found better implementation by keeping implementation from IVehicle

    public abstract void drive(int kilometers);
    //Abstract methods cannot have a body

     **/

    public void printInfo(){
        System.out.println("Vehicle #"+id+": \""+name+"\" by "+brand.getName()+".");
        System.out.println("Available workshops:");
        for(Workshop w : workshops) System.out.println(w.getName());
        System.out.println("Current weight: "+weight+", max weight: "+ maxPermissableWeight);
        System.out.println("Current speed: "+speed+", max speed: "+maxSpeed);
        System.out.println('\n');
    }

    public Workshop getWorkshop(int postcode){
        for(Workshop w : workshops){
            if(w.getPostcode() == postcode) return w;
        }
        return null;
    }


    //getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public ArrayList<Workshop> getWorkshops() {
        return workshops;
    }

    public void setWorkshops(ArrayList<Workshop> workshops) {
        this.workshops = workshops;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getMaxPermissableWeight() {
        return maxPermissableWeight;
    }

    public void setMaxPermissableWeight(int maxPermissableWeight) {
        this.maxPermissableWeight = maxPermissableWeight;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    private static long getNextId(){return ++nextId;}
}
