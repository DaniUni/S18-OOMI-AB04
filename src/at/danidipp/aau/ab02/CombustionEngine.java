package at.danidipp.aau.ab02;

public interface CombustionEngine extends IVehicle{

    public double getFuel();
    public void setFuel(double fuel);
    public double getMaxFuel();
    public void setMaxFuel(double maxFuel);
    public double getFuelConsumption();
    public void setFuelConsumption(double fuelConsumption);

    @Override
    public default boolean keepDriving(int distanceDriven){
        if (getFuel() >= getFuelConsumption()/100) setFuel(getFuelConsumption()/100);
        else {
            System.out.println("Out of fuel! Drove " + distanceDriven + " kilometers.");
            return false;
        }
        return true;
    }

    public default void fillUp(double fuel){
        setFuel(fuel);
        if(getFuel() > getMaxFuel()) setFuel(getMaxFuel());
    }

    public default void printInfo(){
        System.out.println("Current fuel: "+getFuel()+", max fuel: "+getMaxFuel()+", fuel consumption: "+getFuelConsumption());
    }
}
